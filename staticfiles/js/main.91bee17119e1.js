//Loader
var myVar;

    function myFunction() {
        console.log("masuk");
        myVar = setTimeout(showPage, 3000);
    }

    function showPage() {
    document.getElementById("centered").style.display = "none";
    document.getElementById("myCodes").style.display = "block";

    }







//STORY 8
$(document).ready(function(){

    //Accordion in Profile
    var acc = $('.accordion');
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");

            /* Toggle between hiding and showing the active panel */
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
         });
    }


    

     //Change Theme
    $("#changetheme").click(function(){
        $("body").css("background-color", "#10171e");

        //buttons
        $("#changetheme").removeClass("btn-outline-warning");
        $("#changetheme").addClass("btn-danger");
        $("#changeback").removeClass("btn-outline-dark");
        $("#changeback").addClass("btn-danger");
        $("#fifthpage").css("background-color", "black");
        
        // introduction        
        $("#mainpage").css("background-color", "#a2f2ec");
        $("#mainpage").css("color", "#3c101d");
        $('.imagesaya').attr('src','https://i.imgur.com/RSYsjbw.jpg');
        
        //about me
        $("#secondpage").css("background-color", "#5a443c");
        $("#secondpage").css("border", "white");
        $("#secondpage").css("color", "#ffe7e0");

        //accordion
        $(".accordion").css("background-color", "#563c4c");
        $(".accordion").css("color", "white");
        $(".panel").css("background-color", "#b0a8d6");
        
        //education
        $("#fourthpage").css("background-color", "#FCF8C9");
        $(".timeline").css("color", "black");
    });
      

    $("#changeback").click(function(){
        $("body").css("background-color", "pink");  

        // introduction
        $("#mainpage").css("background-color", "#856559");
        $("#mainpage").css("color", "white");
        $('.imagesaya').attr('src','https://i.imgur.com/RSYsjbw.jpg');

        //about me
        $("#secondpage").css("background-color", "#d46767");
        $("#secondpage").css("border", "black");
        $("#secondpage").css("color", "	#dddddd");
        
        //accordion
        $(".accordion").css("background-color", "#d2926f");
        $(".accordion").css("color", "black");
        $(".panel").css("background-color", "#ffe7e0");
         
        //education
        $("#fourthpage").css("background-color", "#c9b2f6");
        $(".timeline").css("color", "black");

        //buttons
        $("#fifthpage").css("background-color", "#85c589");
        $("#changetheme").removeClass("btn-danger");
        $("#changetheme").addClass("btn-dark");
        $("#changeback").removeClass("btn-danger");
        $("#changeback").addClass("btn-dark");
        
    });
});


//STORY 9
//Counter for fav books
var counter = 0;
function addfavorite(caller) {
    if( $(caller).attr('src') == favpic) {
        $(caller).attr('src', defaultpic);
        counter--;
        $("#statusBuku").html(counter);
        console.log("berhasil add fav")
    } else {
        $(caller).attr('src', favpic);
        counter++;
        $("#statusBuku").html(counter);
        console.log("berhasil remove fav")
    }
}
    
//For Books
var dataLength = 0;
// function panggil(arg) {
//     $(document).ready(function(){
//         $.ajax({
//             url: "/getbooks/?search=" + arg,
//             success: function(result){
//                 result = result.items
//                 console.log(result)
//                 var head = "<thead><tr><th>Cover</th><th>Title</th><th>Author</th><th>Description</th><th>Published Date</th><th></th></tr></thead>";
//                     $("#booktable").append(head);
//                     $("#booktable").append("<tbody>");
//                     dataLength = result.length;
//                 for(i=0; i<result.length; i++){
//                     if( typeof result[i].volumeInfo.imageLinks.thumbnail == "undefined") {
//                         var thumbnail = "<td><img src='http://www.bookzart.com/desktop/images/default-book-cover.png'></td>" 
//                     } else {
//                         var thumbnail = "<td><img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'></td>" 
//                     }
//                     var tmp = "<tr>" + thumbnail +
//                     "<td>" + result[i].volumeInfo.title + 
//                     "</td><td>" + result[i].volumeInfo.authors +
//                     "</td><td style='text-align: left;'>" + result[i].volumeInfo.description + 
//                     "</td><td>" + result[i].volumeInfo.publishedDate + 
//                     "</td><td>"+ "<img src='" + defaultpic + "' width='30' height='30' onclick='addfavorite(this)'></td></tr>"
//                     $("#booktable").append(tmp);
//                 }
//                 $("#booktable").append("</tbody>");
//             }
//         });
//     });
// }

//Challenge for books
function getChallenge() {
    var txt = $("#myInput").val();
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();

    $.ajax({
        method: 'GET',
        url: "/getbooks/?search=" + txt,
        headers: {
            "X-CSRFToken": csrftoken,
        },
        success: function (result) {
            $("#booktable thead").remove();
            $('#booktable tbody').remove();
            result = result.items
            console.log(result)
            var head = "<thead><tr><th>Cover</th><th>Title</th><th>Author</th><th>Description</th><th>Published Date</th><th></th></tr></thead>";
                    $("#booktable").append(head);
                    $("#booktable").append("<tbody>");
                    dataLength = result.length;
            for(i=0; i<result.length; i++){
                if( typeof result[i].volumeInfo.imageLinks.thumbnail == "undefined") {
                    var thumbnail = "<td><img src='http://www.bookzart.com/desktop/images/default-book-cover.png'></td>" 
                } else {
                    var thumbnail = "<td><img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'></td>" 
                }
                var tmp = "<tr>" + thumbnail +
                "<td>" + result[i].volumeInfo.title + 
                "</td><td>" + result[i].volumeInfo.authors +
                "</td><td style='text-align: left;'>" + result[i].volumeInfo.description + 
                "</td><td>" + result[i].volumeInfo.publishedDate + 
                "</td><td>"+ "<img src='" + defaultpic + "' width='30' height='30' onclick='addfavorite(this)'></td></tr>"
                $("#booktable").append(tmp);
            }
            $("#booktable").append("</tbody>");
        }
    })
}

//STORY 9
//For Button change to show the tables
$(document).ready(function(){
    $("#button1").click(function () {
        // $('tbody').empty();
        $("#booktable thead").remove();
        $("#booktable tbody").remove();
        counter = 0;
        $("#statusBuku").html(counter);
        panggil('quilting');
        console.log("quilting kepanggil")
    });
    $("#button2").click(function () {
        $("#booktable thead").remove();
        $("#booktable tbody").remove();
        counter = 0;
        $("#statusBuku").html(counter);
        panggil('comic');
        console.log("comic kepanggil")
    });
    $("#button3").click(function () {
        $("#booktable thead").remove();
        $("#booktable tbody").remove();
        counter = 0;
        $("#statusBuku").html(counter);
        panggil('architecture');
        console.log("architecture kepanggil")
    });

});

//==================================================================================================================
//STORY 10
//Register Function
$(document).ready(function(){
	var name = "";
	var password = "";
	$("#id_name").change(function(){
		name = $(this).val();
    })
    $("#id_password").change(function(){
		password = $(this).val();
	});
	$("#id_email").change(function(){
		var email = $(this).val();
		console.log(email)
		$.ajax({
			url: "/validate",
			data:{'email':email,},
			method: "POST",
			dataType: 'json',
			success: function(data){
				console.log(data)
				if(data.not_valid){
					alert("This email has already been taken");
				}
				else{
                    alert("This email is available!");
					if(name.length > 0 && password.length>7){
                        console.log("masuk if remove disabled")
						$("#submit_button").removeAttr("disabled");
					}
				}
			}
		});
    })
    
	$("#submit_button").click(function(){
		event.preventDefault(); 
		$.ajax({
			headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
			url: "/success",
			data: $("form").serialize(),
			method: 'POST',
			dataType: 'json',
			success: function(data){
				swal({
					title: "Welcome " + data.name,
					text: "Thanks for subscribing!",
					type: 'success',
					showConfirmButton: false,
                    timer: 2500
                }).then(function() {
                    window.location.reload();
				});
			}, error: function(data){
				console.log(data);
				swal({
					type: 'error',
					title: 'Sorry',
					text: 'Something is wrong! Please try again',
                    showConfirmButton: false,
                    timer: 2500
                }).then(function() {
                    window.location.reload();
                });			
            }
        })
    });
    
    $.ajax({
        url: "/success",
        success: function(result){
            // result = result.items
            console.log(result)
            var head = "<thead style='color: black;'><tr><th>Name</th><th>Email</th><th></th></tr></thead>";
            $("#subscriberstable").append(head);
            // dataLength = result.length;
            var res = JSON.parse(result)
            console.log(res)
            for(i=0; i<res.length; i++){
                $("#subscriberstable").append("<tbody>");
                console.log(res[i].pk)
                var tmp = "<tr><td style='width: 40%'>" + res[i].fields.name + 
                "</td><td>" + res[i].fields.email + "</td>"+
                "<td><button id='unsubscribe_button' type='submit' class='btn btn-dark' onclick='unsubscribe(" + res[i].pk + ")'>Unsubscribe!</button></td></tr>"
                // <a href='javascript:void(0)' onclick='unsubscribe(" + res[i].pk + ")'>Unsubscribe!</a>
                
                $("#subscriberstable").append(tmp);
            }
            $("#subscriberstable").append("</tbody>");

        }
    })
});



//Challenge Story 10: nampilin list subscriber


function unsubscribe(pk) {
    console.log(pk + " ini emailnya yang jadi id"); 
    $.ajax({
        method: 'POST',
        url:"/deletesubscriber",
        data: {'pk': pk},
        // dataType: 'json',
        success: function() {
            console.log("masuk ke succes function delete")
            swal({
                title: "Success",
                text: 'Please subscribe again!',
                type: 'success',
                showConfirmButton: false,
                timer: 2500
                }).then(function() {
                    window.location.reload();
                });   
        }
    });

}



