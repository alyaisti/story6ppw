//Loader
var myVar;

    function myFunction() {
        console.log("masuk");
        myVar = setTimeout(showPage, 3000);
    }

    function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myCodes").style.display = "block";

    }

//Counter for fav books
var counter = 0;
function addfavorite(caller) {
        if( $(caller).attr('src') == favpic) {
            $(caller).attr('src', defaultpic);
            counter--;
            $("#statusBuku").html(counter);
            console.log("berhasil add fav")
        } else {
            $(caller).attr('src', favpic);
            counter++;
            $("#statusBuku").html(counter);
            console.log("berhasil remove fav")
        }
    }
    

    
$(document).ready(function(){

    //Accordion in Profile
    var acc = $('.accordion');
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            /* Toggle between adding and removing the "active" class,
            to highlight the button that controls the panel */
            this.classList.toggle("active");

            /* Toggle between hiding and showing the active panel */
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
         });
    }
    

     //Change Theme
    $("#changetheme").click(function(){
        $("body").css("background-color", "#10171e");

        //buttons
        $("#changetheme").removeClass("btn-outline-warning");
        $("#changetheme").addClass("btn-danger");
        $("#changeback").removeClass("btn-outline-dark");
        $("#changeback").addClass("btn-danger");
        $("#fifthpage").css("background-color", "black");
        
        // introduction        
        $("#mainpage").css("background-color", "#a2f2ec");
        $("#mainpage").css("color", "#3c101d");
        $('.imagesaya').attr('src','https://i.imgur.com/RSYsjbw.jpg');
        
        //about me
        $("#secondpage").css("background-color", "#5a443c");
        $("#secondpage").css("border", "white");
        $("#secondpage").css("color", "#ffe7e0");

        //accordion
        $(".accordion").css("background-color", "#563c4c");
        $(".accordion").css("color", "white");
        $(".panel").css("background-color", "#b0a8d6");
        
        //education
        $("#fourthpage").css("background-color", "#FCF8C9");
        $(".timeline").css("color", "black");
        

    });
      

    $("#changeback").click(function(){
        $("body").css("background-color", "pink");  
        
        
        
        // introduction
        
        $("#mainpage").css("background-color", "#856559");
        $("#mainpage").css("color", "white");
        $('.imagesaya').attr('src','https://i.imgur.com/RSYsjbw.jpg');
        

        //about me
        $("#secondpage").css("background-color", "#d46767");
        $("#secondpage").css("border", "black");
        $("#secondpage").css("color", "	#dddddd");
        

        //accordion
        $(".accordion").css("background-color", "#d2926f");
        $(".accordion").css("color", "black");
        $(".panel").css("background-color", "#ffe7e0");
        
        
        //education
        $("#fourthpage").css("background-color", "#c9b2f6");
        $(".timeline").css("color", "black");

        //buttons
        $("#fifthpage").css("background-color", "#85c589");
        $("#changetheme").removeClass("btn-danger");
        $("#changetheme").addClass("btn-dark");
        $("#changeback").removeClass("btn-danger");
        $("#changeback").addClass("btn-dark");
        
        
    });

});


