//Loader
var myVar;

    function myFunction() {
        console.log("masuk");
        myVar = setTimeout(showPage, 3000);
    }

    function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myCodes").style.display = "block";

    }

//Counter for fav books
var counter = 0;
    function addfavorite(caller) {
        if( $(caller).attr('src') == favpic) {
            $(caller).attr('src', defaultpic);
            counter--;
            $("#statusBuku").html(counter);
            console.log("berhasil add fav")
        } else {
            $(caller).attr('src', favpic);
            counter++;
            $("#statusBuku").html(counter);
            console.log("berhasil remove fav")
        }
    }
    

var dataLength = 0;

function panggil(arg) {
    $(document).ready(function(){
    // $(function(){
        $.ajax({
            url: "/getbooks/?search=" + arg,
            success: function(result){
                result = result.items
                console.log(result)
                var head = "<thead><tr><th>Cover</th><th>Title</th><th>Author</th><th>Description</th><th>Published Date</th><th></th></tr></thead>";
                    $("#booktable").append(head);
                    $("#booktable").append("<tbody>");
                    dataLength = result.length;
                for(i=0; i<result.length; i++){
                    if( typeof result[i].volumeInfo.imageLinks.thumbnail == undefined) {
                        var thumbnail = "<td><img src='http://www.bookzart.com/desktop/images/default-book-cover.png'></td>" 
                    } else {
                        var thumbnail = "<td><img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'></td>" 
                    }

                    var tmp = "<tr>" + thumbnail +
                    "<td>" + result[i].volumeInfo.title + 
                    "</td><td>" + result[i].volumeInfo.authors +
                    "</td><td style='text-align: left;'>" + result[i].volumeInfo.description + 
                    "</td><td>" + result[i].volumeInfo.publishedDate + 
                    "</td><td>"+ "<img src='" + defaultpic + "' width='30' height='30' onclick='addfavorite(this)'></td></tr>"
                    $("#booktable").append(tmp);
                }
                $("#booktable").append("</tbody>");
            }
        });
    });
}

var pageCounter = 1;
var button1 = document.getElementById("button1");
var button2 = document.getElementById("button2");
var button3 = document.getElementById("button3");

// button1.addEventListener("click",
//     $(document).ready(function(){
//     var thisRequest = new XMLHttpRequest();
//     thisRequest.ope 

//     }


// )

$(document).ready(function(){
    $("#button1").click(function () {
        $("#booktable th").remove();
        $("#booktable tr").remove();
        counter = 0;
        $("#statusBuku").html(counter);
        panggil('quilting');
        console.log("quilting kepanggil")
    });
    $("#button2").click(function () {
        $("#booktable th").remove();
        $("#booktable tr").remove();
        counter = 0;
        $("#statusBuku").html(counter);
        panggil('comic');
        console.log("comic kepanggil")
    });
    $("#button3").click(function () {
        $("#booktable th").remove();
        $("#booktable tr").remove();
        counter = 0;
        $("#statusBuku").html(counter);
        panggil('architecture');
        console.log("architecture kepanggil")
    });

});
    
$(document).ready(function(){

    //Accordion in Profile
    var acc = $('.accordion');
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            /* Toggle between adding and removing the "active" class,
            to highlight the button that controls the panel */
            this.classList.toggle("active");

            /* Toggle between hiding and showing the active panel */
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
         });
    }
    

     //Change Theme
    $("#changetheme").click(function(){
        $("body").css("background-color", "#10171e");

        //buttons
        $("#changetheme").removeClass("btn-outline-warning");
        $("#changetheme").addClass("btn-danger");
        $("#changeback").removeClass("btn-outline-dark");
        $("#changeback").addClass("btn-danger");
        $("#fifthpage").css("background-color", "black");
        
        // introduction        
        $("#mainpage").css("background-color", "#a2f2ec");
        $("#mainpage").css("color", "#3c101d");
        $('.imagesaya').attr('src','https://i.imgur.com/RSYsjbw.jpg');
        
        //about me
        $("#secondpage").css("background-color", "#5a443c");
        $("#secondpage").css("border", "white");
        $("#secondpage").css("color", "#ffe7e0");

        //accordion
        $(".accordion").css("background-color", "#563c4c");
        $(".accordion").css("color", "white");
        $(".panel").css("background-color", "#b0a8d6");
        
        //education
        $("#fourthpage").css("background-color", "#FCF8C9");
        $(".timeline").css("color", "black");
        

    });
      

    $("#changeback").click(function(){
        $("body").css("background-color", "pink");  
        
        
        
        // introduction
        
        $("#mainpage").css("background-color", "#856559");
        $("#mainpage").css("color", "white");
        $('.imagesaya').attr('src','https://i.imgur.com/RSYsjbw.jpg');
        

        //about me
        $("#secondpage").css("background-color", "#d46767");
        $("#secondpage").css("border", "black");
        $("#secondpage").css("color", "	#dddddd");
        

        //accordion
        $(".accordion").css("background-color", "#d2926f");
        $(".accordion").css("color", "black");
        $(".panel").css("background-color", "#ffe7e0");
        
        
        //education
        $("#fourthpage").css("background-color", "#c9b2f6");
        $(".timeline").css("color", "black");

        //buttons
        $("#fifthpage").css("background-color", "#85c589");
        $("#changetheme").removeClass("btn-danger");
        $("#changetheme").addClass("btn-dark");
        $("#changeback").removeClass("btn-danger");
        $("#changeback").addClass("btn-dark");
        
        
    });

});


