from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.conf import settings
from django.views.defaults import page_not_found
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, date
import requests
import json
import re
# from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core import serializers
from django.db import IntegrityError

from .forms import HelloWorldForm, SubscriberForm
from .models import HelloWorldModel, FavoriteBookModel, SubscriberModel

# Create your views here.
response = {'author' : "Alya Isti Safira"}

def mainpage(request):
    return render(request, 'app6/mainpage.html', response)

#Story 6
def helloworld(request):
    response['author'] = "Alya Isti Safira" 
    response['addtask'] = HelloWorldForm
    helloworldall = HelloWorldModel.objects.all().values()
    response['helloworldall'] = helloworldall
    return render(request, 'app6/form_field.html', response)

def addtask(request):
    form = HelloWorldForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['title'] = request.POST['title']
        thisworld = HelloWorldModel(title=response['title'])
        thisworld.save()
        return HttpResponseRedirect('/helloworld/')
    else:
        return HttpResponseRedirect('/helloworld/')

def deletetask(request):
    deletehelloworld = HelloWorldModel.objects.all().delete()
    response['deletehelloworld'] = deletehelloworld
    return HttpResponseRedirect('/helloworld/')

curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 6, 23)
entry_date = date(2017, 7, 31)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
  
def profile(request):
    args = {'myAge': calculate_age(birth_date.year), 'author': "Alya Isti Safira"}
    return render(request, 'app6/profile.html', args)



#Story 9

# BOOK_API_URL = "https://www.googleapis.com/books/v1/volumes?q=quilting"
# BOOK_LIST =  requests.get(BOOK_API_URL).json()
# BOOK_ID_LIST = [ BOOK_LIST['items'][book]['id'] for book in range(len(BOOK_LIST['items'])) ]

def hellobooks(request):
    # favoritebook = FavoriteBookModel.objects.all()
    # favoritebookid = [ fav.bookid for fav in favoritebook ]
    # count = FavoriteBookModel.objects.count()
    # response['books'] = BOOK_LIST
    # response['favorited'] = favoritebookid
    # response['count'] = count
    if request.user.is_authenticated:
        request.session['user'] = request.user.username
        request.session['email'] = request.user.email
        request.session.get('counter', 0)
        print(dict(request.session))
        for key, value in request.session.items():
            print('{} => {}'.format(key,value))
    return render(request, 'app6/books.html', response)

def getbooks(request):
    search = request.GET.get('search')
    BOOK_API_URL = "https://www.googleapis.com/books/v1/volumes?q=" + search
    BOOK_LIST =  requests.get(BOOK_API_URL).json()
    BOOK_LIST['status_code'] = 200
    if request.user.is_authenticated:
        count = request.session.get('counter', 0)
        request.session['counter'] = count
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))
    return JsonResponse(BOOK_LIST)

# def countfav(request):
#     favoritebook = FavoriteBookModel.objects.all()
#     favoritebookid = [ fav.book_id for fav in favoritebook ]


#Story 10

# def regist(request):
# 	form = SubscriberForm(request.POST or None)
# 	return render(request,"app6/subscribe.html",{'form':form, 'author' : "Alya Isti Safira"})

# @csrf_exempt
# def addSubscriber(request):
# 	print(request.POST)
# 	if request.method == 'POST':
# 		enctype= "multipart/form-data"
# 		name = request.POST['name']
# 		email = request.POST['email']
# 		password = request.POST['password']
# 		subscriber = SubscriberModel(name = name, email = email, password = password)
# 		subscriber.save()
# 		data = model_to_dict(subscriber)
# 		return HttpResponse(data)

		
# @csrf_exempt
# def validate_email(request):
# 	email_validate_check = False
# 	enctype = "multipart/form-data"
# 	email = request.POST.get('email')
# 	if(len(email))>=1:
# 		email_validate_check = bool(re.match("^.+@(\[?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$",email))

# 	data = {'is_taken':SubscriberModel.objects.filter(email = email).exists(),'is_email_valid':email_validate_check}
# 	return JsonResponse(data)



def subscribe(request):
    response['form'] = SubscriberForm(request.POST or None)
    # allsubscribers = SubscriberModel.objects.all()
    # response['allsubscribers'] = allsubscribers
    return render(request,"app6/subscribe.html",response)



# def subscribe(request):
#     form = SubscriberForm(request.POST)
#     if request.method == 'POST' and form.is_valid():
#         data = form.cleaned_data
#         status_subscribe = True
#         try:
#             SubscriberModel.objects.create(**data)
#         except:
#             status_subscribe = False
#         return JsonResponse({'status_subscribe' : status_subscribe})
#     content = {'form': form}
#     return render(request, 'app6/subscribe.html', content)

    
# def addSubscriber(request):
#     if(request.method == "POST"):
#         try:
#             validate_email(request.POST['email'])
#             subscriber = SubscriberModel.objects.create(name=request.POST['name'],
#                                     email=request.POST['email'],
#                                     password=request.POST['password'])
#             subscriber = serializers.serialize("json", [subscriber, ])
#             print(subscriber)

#         except (ValidationError, IntegrityError)  as e:
#             response['message'] = "Email format is wrong,\
#                                 or email is already exist"
#             return JsonResponse(response, status=200)

#         response['message'] = "Subscriber created"
#         response['subscriber'] = subscriber
#         return JsonResponse(response, status=200)

#     response['message'] = "GET method not allowed"
#     return JsonResponse(response, status=403)


# @csrf_exempt
# def checkEmail(request):
#     if request.method == "POST":
#         email = request.POST['email']
#         is_email_already_exist = Subscriber.objects.filter(pk=email).exists()
#         return JsonResponse({'is_email': is_email_already_exist})

@csrf_exempt
def checkValid(request):
	email = request.POST.get("email")
	data = {'not_valid': SubscriberModel.objects.filter(email__iexact=email).exists()}
	return JsonResponse(data)

def success(request):
    submit_form = SubscriberForm(request.POST or None)
    data = {}
    if(request.method == "POST" and submit_form.is_valid()):
        cd = submit_form.cleaned_data
        newSubscriber = SubscriberModel(name=cd['name'], email=cd['email'], password=cd['password'])
        newSubscriber.save()
        data = {'name': cd['name'], 'email':cd['email'], 'password': cd['password']}
    elif (request.method == "GET"):
        obj = SubscriberModel.objects.all()
        data = serializers.serialize('json',obj)
    return JsonResponse(data, safe=False)


@csrf_exempt
def deletesubscriber(request):
    if (request.method == "POST"):
        print('apus neh')
        primKey = request.POST['pk']
        deletesubscriber = SubscriberModel.objects.filter(pk=primKey).delete()
        response['deletesubscriber'] = deletesubscriber
        return HttpResponseRedirect("/subscribe/")
    else:
        print('wadoh')
        return HttpResponseRedirect("/subscribe/")



#Story 11
def tambah(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = "application/json")

def kurang(request):
    print(dict(request.session))
    request.session['counter'] = request.session['counter'] - 1
    return HttpResponse(request.session['counter'], content_type = "application/json")







