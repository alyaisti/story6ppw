from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from datetime import datetime, date


# Create your models here.

class HelloWorldModel(models.Model):
    title = models.CharField(max_length=300)
    created_date = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    #description = models.TextField(max_length=200)
    #created_date = models.DateTimeField(auto_now_add=True)

class FavoriteBookModel(models.Model):
    #booktitle = models.CharField(max_length=500)
    bookid = models.CharField(max_length=350, unique=True, primary_key=True)

class SubscriberModel(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=100, unique=True)
    password = models.CharField(max_length=50)
