from django.conf.urls import url, include
from django.urls import path
from .views import mainpage, addtask, helloworld, profile, deletetask
from .views import hellobooks, getbooks, tambah, kurang
from .views import subscribe, checkValid, success, deletesubscriber
from django.contrib.auth import views
# from .views import login, logout


urlpatterns = [
    path('', mainpage, name='mainpage'),
    path('', helloworld, name='helloworld'),
    path('addtask', addtask, name='addtask'),
    path('deletetask', deletetask, name='deletetask'),
    path('', profile, name='profile'),
    
    path('', subscribe, name='subscribe'),
    path('', checkValid, name='checkValid'),
    path('', success, name='success'),
    path('deletesubscriber', deletesubscriber, name='deletesubscriber'),

    # path('', login, name='login'),
    # path('', logout, name='logout'),
    # path('login/', views.LoginView.as_view(), name='login'),
    # path('logout/', logoutPage, name='logout'),
    # path('auth/', include('social_django.urls', namespace='social')),
    
    # path('books', views.bookList, name="books"),
	# path('searchJson', views.JSonSearch, name="searchJson"),

    
    path('', hellobooks, name='hellobooks'),
    path('', getbooks, name='getbooks'),
	path('tambah', tambah, name="tambah"),
	path('kurang', kurang, name="kurang"),


]


from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()