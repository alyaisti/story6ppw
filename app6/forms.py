from django import forms
from .models import HelloWorldModel, SubscriberModel
from django.forms import ModelForm


class HelloWorldForm(forms.Form):
    error_messages = {
            'required': 'Please fill this field!',
            'invalid': 'Fill it with your email!',
        }

    title_attrs = {
            'type': 'text',
            'class': 'form-control',
            'placeholder':'Here!'
        }
    
    #description_attrs = {

    #    'cols': 50,
    #    'class': 'todo-form-textarea',
    #}

    #date_attrs = {
    #    'type':'datetime-local', 
    #    'class':'form-control'
    #}

    title = forms.CharField(label='', required=True, max_length=300, widget=forms.TextInput(attrs=title_attrs))
    #description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
    #created_date = forms.DateTimeField(label='Date', required=True,  widget=forms.DateInput(attrs=date_attrs))



class SubscriberForm(forms.Form):
    name_attrs = {
		'type':'text',
		'class':'form-control',
		'placeholder': 'Full Name',
	}

    mail_attrs = {
		'type':'text',
		'class':'form-control',
		'placeholder':'example@mail.com'
	}

    pass_attrs = {
		'type':'password',
		'class':'form-control',
		'placeholder':'Password',
	}
    
    name = forms.CharField(max_length=200, error_messages={"required":"Please enter your name"}, widget= forms.TextInput(attrs=name_attrs))
    email = forms.CharField(required=True, max_length=100, error_messages={"required":"Please enter a valid email"}, widget=forms.TextInput(attrs=mail_attrs))
    password = forms.CharField(required=True, min_length=8, max_length=50, error_messages={"min_length":"Password must be longer than 7 characters"}, widget=forms.PasswordInput(attrs=pass_attrs))


   