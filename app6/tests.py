from django.test import TestCase, Client
from django.conf import settings
from django.urls import resolve
from django.http import request
from django.utils.encoding import force_text
from .views import mainpage, helloworld, addtask, profile, calculate_age, hellobooks, getbooks
from .views import subscribe, checkValid, success
# from .views import *
from .models import HelloWorldModel, FavoriteBookModel, SubscriberModel
from .forms import HelloWorldForm, SubscriberForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest
import requests

# Create your tests here.

# class Story11Test(TestCase):
#     def setUp(self):
#         settings.SESSION_ENGINE = "django.contrib.sessions.backends.file"
#         engine = import_module(settings.SESSION_ENGINE)
#         store = engine.SessionStore()
#         store.save()
#         self.session = store
#         self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key



class Story10Test(TestCase):
    def test_subscribe_page_exist(self):
        response = Client().get('/subscribe')
        self.assertEqual(response.status_code, 200)

    def test_using_subscribe_func(self):
        found = resolve('/subscribe')
        self.assertEqual(found.func, subscribe)

    def test_lab10_using_checkValid_func(self):
        found = resolve('/validate')
        self.assertEqual(found.func, checkValid)

    def test_lab10_using_success_func(self):
        found = resolve('/success')
        self.assertEqual(found.func, success)
    
    def test_subscribe_form_has_placeholder_and_css(self):
        form = SubscriberForm()
        self.assertIn('class="form-control"', form.as_p())

    def test_model_can_create_new_subscriber(self):
        new_subscriber = SubscriberModel.objects.create(name="me", email="itsme@yahoo.com", password="hello1234")
        counting_all_subscriber = SubscriberModel.objects.all().count()
        self.assertEqual(counting_all_subscriber, 1)

    def test_subscribe_form_validation_for_blank_items(self):
        form = SubscriberForm(data={'name':'', 'password':'', 'email':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],['Please enter your name']
        )
        self.assertEqual(
            form.errors['email'],['Please enter a valid email']
        )
        self.assertEqual(
            form.errors['password'],['This field is required.']
        )

    def test_subscribe_pass_less_than_8char(self):
        form = SubscriberForm(data={'name':'alya isti', 'email':'mbekgukguk@gmail.com', 'password':'ahaha',})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password'],["Password must be longer than 7 characters"]
        )

    def test_subscribe_email_has_exist(self):
        name = 'alya isti'
        email = 'mbekgukguk@gmail.com'
        password = 'ahahahakangen'
        postIt = Client().post('/validate', {'email':email})
        # respond it false
        self.assertJSONEqual(force_text(postIt.content), {'not_valid': False})
        # send data to database successfully
        Client().post('/success', {'name': name, 'email':email, 'password': password})
        count_subscriber = SubscriberModel.objects.all().count()
        self.assertEqual(count_subscriber, 1)
        # enter the same email for the second time
        postIt = Client().post('/validate', {'email': email})
        # email already taken so not_valid = True
        self.assertJSONEqual(str(postIt.content, encoding='utf8'), {'not_valid': True})
        # the not valid data is not entered into database
        count_subscriber = SubscriberModel.objects.all().count()
        self.assertEqual(count_subscriber, 1)



class Story9Test(TestCase):
    def test_books_page_exist(self):
        response = Client().get('/hellobooks')
        self.assertEqual(response.status_code, 200)

    def test_using_books_function(self):
        found = resolve('/hellobooks')
        self.assertEqual(found.func, hellobooks)

    def test_get_books_quilting_page_exist(self):
        response = Client().get('/getbooks/?search=quilting')
        self.assertEqual(response.status_code, 200)


    def test_model_instance_created(self):
        counter = FavoriteBookModel.objects.count()
        liked_book = FavoriteBookModel.objects.create(bookid="dsjfhskfj")
        self.assertEqual(counter + 1, FavoriteBookModel.objects.count())

# def test_correct_output(self):
    #     response = Client().get('/hellobooks')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn("Check these books.", html_response)

# class Story7FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25) 
#         super(Story7FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Story7FunctionalTest, self).tearDown()
 
#     def test_input_todo(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/helloworld')
#         title = selenium.find_element_by_id('id_title')
#         submit = selenium.find_element_by_id('submit')
#         title.send_keys('STOORIII TUJUHHHH')
#         submit.send_keys(Keys.RETURN)

#     def test_title_in_home(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/helloworld')
#         time.sleep(5)
#         title = selenium.title
#         self.assertEqual('Diary of Eyebrow!?', title)
    
#     def test_write_link_in_home(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/helloworld')
#         time.sleep(5)
#         addtask = selenium.find_element_by_link_text('Write something!').text
#         self.assertIn(addtask, selenium.page_source)

#     def test_delete_link_in_home(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/helloworld')
#         time.sleep(5)
#         deletetask = selenium.find_element_by_link_text('Delete Table').text
#         self.assertIn(deletetask, selenium.page_source)

#     def test_name_in_profile(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/')
#         time.sleep(5)
#         h1 = selenium.find_element_by_tag_name('h1').text
#         self.assertIn('Alya Isti Safira', h1)

#     def test_jumbotron_has_style(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/helloworld')
#         time.sleep(5)
#         style = selenium.find_element_by_css_selector('div.jumbotron')
        
#     def test_rounded_table_has_style(self):
#         selenium = self.selenium
#         selenium.get('http://localhost:8000/helloworld')
#         time.sleep(5)
#         style = selenium.find_element_by_css_selector('div.roundedrec')



class Story6Test(TestCase):
    def test_main_page_exist(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_using_mainpage_method(self):
        found = resolve('/')
        self.assertEqual(found.func, mainpage)

    def test_helloworld_page_exist(self):
        response = self.client.get("/helloworld")
        self.assertEqual(response.status_code, 200)
    
    def test_using_helloworld_method(self):
        found = resolve('/helloworld')
        self.assertEqual(found.func, helloworld)
    
    def test_has_halo_apa_kabar(self):
        response = Client().get('/helloworld')
        html_response = response.content.decode('utf8')
        self.assertIn("Halo, apa kabar?", html_response)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = HelloWorldModel.objects.create(title='mengerjakan lab ppw')

        # Retrieving all available activity
        counting_all_available_todo = HelloWorldModel.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
    
    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = HelloWorldForm()
        self.assertIn('class="form-control', form.as_p())
        self.assertIn('id="id_title"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = HelloWorldForm(data={'title': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['title'],
            ["This field is required."]
        )
             
    def test_story6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/addtask', {'title': test,})
        self.assertEqual(response_post.status_code, 302)
        response= Client().get('/helloworld')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/addtask', {'title': '',})
        self.assertEqual(response_post.status_code, 302)
        response= Client().get('/helloworld')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_story6_delete_result(self):
        # Creating a new activity to delete
        deleteactivity = HelloWorldModel.objects.create(title='pengen yoshinoya')
        response = Client().post('/deletetask')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(deleteactivity, HelloWorldModel.objects.all())

    def test_profile_page_exist(self):
        response = self.client.get("/profile")
        self.assertEqual(response.status_code, 200)

    def test_using_profile_method(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)
    
    def test_using_calculate_age_method(self):
        self.assertEqual(calculate_age(2000), 18)

    def test_lab6_profile_has_name(self):
        response = Client().get('/profile')
        html_response = response.content.decode('utf8')
        self.assertIn("Alya Isti Safira", html_response)

    

