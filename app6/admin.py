from django.contrib import admin
from .models import HelloWorldModel, SubscriberModel

# Register your models here.
admin.site.register(HelloWorldModel)
admin.site.register(SubscriberModel)