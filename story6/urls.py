"""story6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls import url
from django.conf.urls.static import static
from app6.views import mainpage as mainpage
from app6.views import helloworld as helloworld
from app6.views import addtask as addtask
from app6.views import profile as profile
from app6.views import deletetask as deletetask
from app6.views import hellobooks as hellobooks
from app6.views import getbooks as getbooks
from app6.views import subscribe as subscribe
from app6.views import checkValid as checkValid
from app6.views import success as success
from app6.views import deletesubscriber as deletesubscriber
from app6.views import tambah as tambah
from app6.views import kurang as kurang
from django.contrib.auth import views
from .views import logoutPage

import app6.urls as app6


urlpatterns = [
    path('admin/', admin.site.urls),
    
    #Story 6
    re_path(r'^$', mainpage, name="mainpage"),
    re_path(r'^helloworld', helloworld, name="helloworld"),
    re_path(r'^addtask', addtask, name="addtask"),
    re_path(r'^deletetask', deletetask, name="deletetask"),
    re_path(r'^profile', profile, name="profile"),

    #Story 9
    re_path(r'^hellobooks', hellobooks, name="hellobooks"),
    re_path(r'^getbooks', getbooks, name="getbooks"),
    
    #Story 10
    re_path(r'^subscribe', subscribe, name="subscribe"),
    re_path(r'^validate', checkValid, name="validate"),
    re_path(r'^success', success, name="success"),
    re_path(r'^deletesubscriber', deletesubscriber, name="deletesubscriber"),

    #Story 11
    re_path(r'^login', views.LoginView.as_view(), name="login"),
    re_path(r'^logout', logoutPage, name="logout"),
    re_path(r'^auth', include('social_django.urls', namespace='social')),


    re_path(r'^tambah', tambah, name="tambah"),
    re_path(r'^kurang', kurang, name="kurang"),
    # path('login/', views.LoginView.as_view(), name='login'),
    # path('logout/', logoutPage, name='logout'),
    # path('auth/', include('social_django.urls', namespace='social')),
    
    

   
]
