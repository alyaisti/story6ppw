# Repository untuk Story 6, 7, 8, 9
# Semua dalam satu app: app6.

```bash
Nama    :   Alya Isti Safira
NPM     :   1706984511
Kelas   :   PPW F
```
#Link Website Herokuapp
[https://story6-alyaisti.herokuapp.com]

#Status Website
[![pipeline status](https://gitlab.com/alyaisti/story6ppw/badges/master/pipeline.svg)](https://gitlab.com/alyaisti/story6ppw/commits/master)
[![coverage report](https://gitlab.com/alyaisti/story6ppw/badges/master/coverage.svg)](https://gitlab.com/alyaisti/story6ppw/commits/master)